<?php

session_start();
if(!isset($_SESSION['login']))
{
   header('location: logout.php');
}
include('config.php');
$name ='';
$UserName ='';
$Password ='';
$update=false;
$id=0;

$connect = new mysqli($host, $db_user, $db_password, $db);

$query = "SELECT * FROM tblrole";
$result = mysqli_query ($connect, $query);


if(isset($_POST["insert"]))
{

    $name =$_POST['Name'];
    $UserName =$_POST['UserName'];
    $Password =$_POST['Password'];
    $role =$_POST['role'];
    $query="Insert into tbllogin (Name,UserName,Password,RoleID) values('$name','$UserName','$Password','$role')";
    $res=mysqli_query($connect,$query);
}

if(isset($_POST["Update"]))
{

    $id=$_POST['id'];
    $name =$_POST['Name'];
    $UserName =$_POST['UserName'];
    $Password =$_POST['Password'];
    $role =$_POST['role'];
    $query="update tbllogin set Name='$name',UserName='$UserName',PASSWORD='$Password',RoleID='$role' where LoginID=$id";
    $res=mysqli_query($connect,$query);
    header('location: UserManagement.php');
}

if(isset($_GET['delete'])){
    $id=$_GET['delete'];
    $query="delete from tbllogin where LoginId=$id";
    $res=mysqli_query($connect,$query);
}

if(isset($_GET['edit'])){
    $id=$_GET['edit'];
    $update=true;
    $query="select * from tbllogin where LoginId=$id";
    $res=mysqli_query($connect,$query);

    if(count(array($res))==1){
        $row=$res->fetch_array();
        $name =$row['Name'];
        $UserName =$row['UserName'];
        $Password =$row['PASSWORD'];
        $role =$row['RoleID'];
    }
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <style>table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }
      
      td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
      }
      
      tr:nth-child(even) {
        background-color: #dddddd;
      }</style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Blank</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    
                </div>
                <div class="sidebar-brand-text mx-3">MAGELLAN</sup></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            

           

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
           

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link" href="ViewRecords.php">
                <i class="fa fa-search"></i>
                    <span>View Records</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="FileUpload.php">
                    <i class="fa fa-upload"></i>
                    <span>File Upload</span></a>
            </li>

         

            <li class="nav-item">
                <a class="nav-link" href="UserManagement.php">
                    <i class="fa fa-users"></i>
                    <span>User Management</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ChangePassword.php">
                    <i class="fa fa-key"></i>
                    <span>Change Password</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.php">
                    <i class="fa fa-eject"></i>
                    <span>Logout</span></a>
            </li>

           


            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <h3>User Management</h3>
                    

                    
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                <form method="post" action="UserManagement.php">
                    <!-- Page Heading -->
    
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <div class="col-sm-6 form-group">
                            <label for="">Name:</label>
                            <input type="text" class="form-control form-control-user"
                                name="Name" value="<?php echo $name ?>" requireed placeholder="Name">
                        </div>
                        
                        <div class="col-sm-6 form-group">
                            <label for="">User Name:</label>
                            <input type="text" class="form-control form-control-user"
                            name="UserName" value="<?php echo $UserName  ?>" requireed placeholder="User Name">
                        </div>
                                          
                        <div class="col-sm-6 form-group">
                            <label for="">Password:</label>
                            <input type="text" class="form-control form-control-user"
                            name="Password" value="<?php echo $Password  ?>"  requireed placeholder="Passowrd">
                        </div>
                        
                        <div class="col-sm-6 form-group">
                            <label for="">Role:</label>
                            <select id="role" name="role" class="form-control">
                                <option  value="1">Admin</option>
                                <option value="2">User</option>
                               
                              </select>
                        </div>
                       

                        <div class="form-group">
                            &nbsp; 

                            <?php 
                            if($update==true):
                            ?>
                                 <button type="submit" class="btn btn-primary" name="Update" >Update</button>
                            <?php else: ?>
                                <button type="submit" class="btn btn-primary" name="insert" >Insert</button>
                            <?php endif; ?>  
                        </div>
                    </form>
                      
                <?php
                
                $mysqli=new mysqli($host, $db_user, $db_password, $db) or die(mysqli_error($mysqli));
                $result=$mysqli->query("SELECT LoginID,Name,UserName,PASSWORD,RoleID from tbllogin") or die($mysqli -> error);
                
                ?>
                

                <div class="row justify-content-center table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>LoginID</th>
                            <th>Full Name</th>
                            <th>User Name</th>
                            <th>Password</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    <?php
                    while($row=$result->fetch_assoc()): ?>
                    <tr>
                    <td><?php echo $row['LoginID']; ?></td>
                    <td><?php echo $row['Name']; ?></td>
                    <td><?php echo $row['UserName']; ?></td>
                    <td><?php echo $row['PASSWORD']; ?></td>
                    <td><?php echo $row['RoleID']; ?></td>
                    <td>
                    <a href="UserManagement.php?edit=<?php echo $row['LoginID'];?>" class="btn btn-primary"> Edit</a> 
                    <a href="UserManagement.php?delete=<?php echo $row['LoginID'];?>" class="btn btn-danger"> Delete</a> 
                    </td>
                    </tr>
                    <?php endwhile; ?>
                    
                   
                
                     </table>
                
                </div>
                
                
                
  


                
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                   
                </div>
                
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="index.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>