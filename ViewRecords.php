<?php
session_start();
if(!isset($_SESSION['login']))
{
   header('location: logout.php');
}
include('config.php');
$colname ='';
$operation ='';
$text ='';
$oprcode='';
$action='';
/*
$connect = mysqli_connect("localhost", "root", "");
mysqli_select_db($connect, "import_excel");
*/

$conn =new mysqli($host, $db_user, $db_password, $db);


$query='';
$row='';
if(isset($_POST["submit"]))
{
    //var_dump($_POST); die();

    $action=true;
    $colname =$_POST['col'];
    $operation =$_POST['opr'];
    $text =$_POST['searchtext'];
    

    if($operation=="Exact Match"){
        
        $query = "SELECT * FROM tblmasterdata WHERE $colname = '".$text."'";
        $result = mysqli_query ($conn, $query)or die($query->error);
       

      

    }else if($operation=="Starts With"){
        $query = "SELECT * FROM tblmasterdata WHERE $colname like '".$text."%'";
        $result = mysqli_query ($conn, $query)or die($query->error); 
        
        
       
    }else if($operation=="Ends With"){
        $query = "SELECT * FROM tblmasterdata WHERE $colname like '%".$text."'";
        $result = mysqli_query ($conn, $query)or die($query->error); 
       
        
      
    }else if($operation=="Approximate Match"){
    
        $query = "SELECT * FROM tblmasterdata WHERE $colname like '%".$text."%'";
        $result = mysqli_query ($conn, $query)or die($query->error); 
        

    } else{
        echo "Invalid !";
    }
         
}
      
   //  var_dump($result);   die(); 
     
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <style>table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }
      
      td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
      }
      
      tr:nth-child(even) {
        background-color: #dddddd;
      }</style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Blank</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    
                </div>
                <div class="sidebar-brand-text mx-3">MAGELLAN</sup></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->




<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->


<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
    <a class="nav-link" href="ViewRecords.php">
    <i class="fa fa-search"></i>
        <span>View Records</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="FileUpload.php">
        <i class="fa fa-upload"></i>
        <span>File Upload</span></a>
</li>



<li class="nav-item">
    <a class="nav-link" href="UserManagement.php">
        <i class="fa fa-users"></i>
        <span>User Management</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="ChangePassword.php">
        <i class="fa fa-key"></i>
        <span>Change Password</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="logout.php">
        <i class="fa fa-eject"></i>
        <span>Logout</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">
</ul>
<!-- End of Sidebar -->

        
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <h3>Search Records</h3>
                    

                    
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                <form method="post" action="ViewRecords.php">
                    <!-- Page Heading -->
                    
                    <br>    <br>

                    <div class="col-sm-6 form-group">
                            <label for="">Field Name:</label>
                            <select id="col" name="col" class="form-control">
                                <option  value="Holder_ID">Holder_ID</option>
                                <option value="Holder_Name">Holder_Name</option>
                                <option  value="Property_ID">Property_ID</option>
                                <option value="Owner_Name">Owner_Name</option>
                                <option  value="Address_1">Address_1</option>
                                <option value="Address_2">Address_2</option>
                                <option  value="City">City</option>
                                <option value="State">State</option>
                                <option  value="Zip_Code">Zip_Code</option>
                                <option value="Shares">Shares</option>
                                <option  value="Cash_Remmitted">Cash_Remmitted</option>
                                <option value="Owner_Count">Owner_Count</option>
                                <option  value="Property_Type_Code">Property_Type_Code</option>
                                <option value="Property_Type">Property_Type</option>
                               
                            </select>
                     </div>
                     <div class="col-sm-6 form-group">
                            <label for="">Operation:</label>
                            <select id="opr" name="opr" class="form-control">
                                <option  value="Starts wiht">Starts With</option>
                                <option value="Ends With">Ends With</option>
                                <option value="Approximate Match">Approximate Match</option>
                                <option  value="Exact Match">Exact Match</option>
                                                             
                            </select>
                     </div>
                     <div class="col-sm-6 form-group">
                            <label for="">Enter Text to Search:</label>
                            <input type="text" class="form-control form-control-user"
                            name="searchtext"   requireed placeholder="enter text">
                      
                     </div>
                    
                </div>
                <div class="form-group">
                &nbsp;  &nbsp;  &nbsp;  &nbsp; 
                <button type="submit" class="btn btn-primary" name="submit" >Search</button>
                </div>
                </form>
                <div class="form -group">
                <div class="table-responsive" style="overflow-x:scroll">
                &nbsp;  &nbsp; 
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Holder ID</th>
                            <th>Holder Name</th>
                            <th>Property ID</th>
                            <th>Owner Name</th>
                            <th>Address_1</th>
                            <th>Address_2</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip Code</th>
                            <th>Shares</th>
                            <th>Cash Remmitted</th>
                            <th>Owner Count</th>
                            <th>Property Type Code</th>
                            <th>Property Type</th>
                            <th>Status</th>
                            <th>Import Date</th>
                        </tr>
                        </thead>
                    <?php
                    if($action==true)
                  
                       while($row=$result->fetch_assoc()): ?>
                    <tr>
                    <td><?php echo $row['ID']; ?></td>
                    <td><?php echo $row['Holder_ID']; ?></td>
                    <td><?php echo $row['Holder_Name']; ?></td>
                    <td><?php echo $row['Property_ID']; ?></td>
                    <td><?php echo $row['Owner_Name']; ?></td>
                    <td><?php echo $row['Address_1']; ?></td>
                    <td><?php echo $row['Address_2']; ?></td>
                    <td><?php echo $row['City']; ?></td>
                    <td><?php echo $row['State']; ?></td>
                    <td><?php echo $row['Zip_Code']; ?></td>
                    <td><?php echo $row['Shares']; ?></td>
                    <td><?php echo $row['Cash_Remmitted']; ?></td>
                    <td><?php echo $row['Owner_Count']; ?></td>
                    <td><?php echo $row['Property_Type_Code']; ?></td>
                    <td><?php echo $row['Property_Type']; ?></td>
                    <td><?php echo $row['STATUS']; ?></td>
                    <td><?php echo $row['DATE_ADDED']; ?></td>
                    <!-- <td>
                    <a href="ViewRecords.php?delete=<?php echo $row['ID'];?>" class="btn btn-danger"> Delete</a> 
                    </td> -->
                    </tr>
                    <?php endwhile; ?>
                    
                   
                
                     </table>
                
                </div>
                
                </div>

                
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                   
                </div>
                
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="index.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>